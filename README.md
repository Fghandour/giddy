This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

Clone this repo

To install dependencies:

### `npm install`
To run project dependencies:

### `npm run start`

To build project dependencies:

### `npm run build`
