
import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./components/Signin/LoginPage";
import SignUp from "../src/components/Signup/Signup";
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';

import {
  createStore,applyMiddleware, compose } from 'redux';

// redux-logger is a middleware that lets you log every state change
import logger from 'redux-logger';
import Home from "./components/Home/Home";
import 'bootstrap/dist/css/bootstrap.css';
// redux-thunk is a middleware that lets you dispatch async actions
import thunk from 'redux-thunk';
import rootReducer from './components/reducers';
const middleware = applyMiddleware(thunk, logger);
const store = createStore(rootReducer,middleware);


function App() {

    return (
      <Provider store={store}>
        <Home/>
  </Provider>

);
}

export default App;
