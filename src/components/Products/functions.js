import service from "../../service/service";

export async function sendNotification(notification) {
  let formData = new FormData();
  formData.append("email", notification.email);
  var covertProduct=[];
  notification.products.forEach(product => {
    var newObj=JSON.stringify(product)
    covertProduct.push(newObj)
  });
  formData.append("products",covertProduct.join("--"));
  formData.append("TotalPrice", notification.TotalPrice);
  var data = await service.post("notification", formData);
  return data;
}
