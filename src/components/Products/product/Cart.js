import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { removeItem,addQuantity,subtractQuantity} from '../actions'
import Recipe from './Recipe'
import Icon from "react-eva-icons";
class Cart extends Component{

  //to remove the item completely
  handleRemove = (id)=>{
    this.props.removeItem(id);
  }
  //to add the quantity
  handleAddQuantity = (id)=>{
    this.props.addQuantity(id);
  }
  //to substruct from the quantity
  handleSubtractQuantity = (id)=>{
    this.props.subtractQuantity(id);
  }
  render(){

    let addedItems = this.props.items.length ?
      (
        this.props.items.map(item=>{
          return(

            <li className="collection-item avatar" key={item.id}>
              <div className={"row"}>
                <div className="item-img col-4">
                  <img src={item.img} alt={item.img} className="rounded img-thumbnail"/>
                </div>
                <div className="item-desc  col-8">
                  <div className={"d-flex justify-content-between align-items-center"}>
                  <span className="title">{item.title}</span>
                  <span onClick={()=>{this.handleRemove(item.id)}}>
                  <Icon
                    name="trash"
                    size="xlarge"     // small, medium, large, xlarge
                    fill={"red"}
                    animation={{
                      type: "pulse",  // zoom, pulse, shake, flip
                      hover: true,
                      infinite: false
                    }}

                  />
                  </span>
                  </div>
                  <p>{item.desc}</p>
                  <p><b>Price: {item.price}$</b></p>
                  <div className="add-remove">
                  <span to="/cart" onClick={()=>{this.handleAddQuantity(item.id)}}>
                    <Icon
                      name="plus-circle-outline"
                      size="medium"     // small, medium, large, xlarge
                      fill={"blue"}
                      animation={{
                        type: "pulse",  // zoom, pulse, shake, flip
                        hover: true,
                        infinite: false
                      }}

                    />
                  </span>
                    <span className={"m-3"}>
                      <b>Quantity: {item.quantity}</b>
                    </span>
                    <span to="/cart" onClick={()=>{this.handleSubtractQuantity(item.id)}}>

                  <Icon
                    name="minus-circle-outline"
                    size="medium"     // small, medium, large, xlarge
                    fill={"blue"}
                    animation={{
                      type: "pulse",  // zoom, pulse, shake, flip
                      hover: true,
                      infinite: false
                    }}

                  />
                  </span>

                  </div>
                </div>
              </div>




            </li>

          )
        })
      ):

      (
        <p>Nothing.</p>
      )
    return(
      <div className="container">
        <div className="cart">
          <label> Products List </label>
          <ul className="collection">
            {addedItems}
          </ul>
        </div>
        <Recipe />
      </div>
    )
  }
}


const mapStateToProps = (state)=>{
  return{
    items: state.products.addedItems,
  }
}
const mapDispatchToProps = (dispatch)=>{
  return{
    removeItem: (id)=>{dispatch(removeItem(id))},
    addQuantity: (id)=>{dispatch(addQuantity(id))},
    subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart)
