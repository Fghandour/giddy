import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addToCart } from '../actions'
import Icon from "react-eva-icons";
import { Link } from "react-router-dom";
import ReactTooltip from 'react-tooltip';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from "react-toasts";

class ProductsList extends Component{

  handleClick = (id)=>{
    this.props.addToCart(id);
    ToastsStore.success("Produit est ajouté au panier");

  }

  render(){
    let itemList = this.props.items.map(item=>{
      return(
        <div className="col-md-4 col-lg-4  col-sm-1 p-3 ">

          <div className="card" key={item.id}>
            <img className="card-img-top" src={item.img} alt={item.title}/>
            <div className="card-body">
              <div className={"d-flex justify-content-between align-items-center"}>
                <span className="card-title">{item.title}</span>
                <span data-tip='' data-for={item.id+'test'} to="/" className="btn-floating halfway-fab waves-effect waves-light red" onClick={()=>{this.handleClick(item.id)}}>
                   <Icon
                     name="plus-circle"
                     size="xlarge"
                     fill={"blue"}
                     animation={{
                       type: "pulse",
                       hover: true,
                       infinite: false
                     }}
                   />
                </span>
                <ReactTooltip id={item.id+'test'} getContent={() => { return "Ajouter au panier" }}/>
              </div>
              <p>{item.desc}</p>
              <p><b>Price: {item.price}$</b></p>
            </div>

          </div>

        </div>


      )
    })

    return(
      <div className="container">
        <div className="box row">
          {itemList}
        </div>
        < ToastsContainer store = { ToastsStore } position = { ToastsContainerPosition.TOP_CENTER }/>
      </div>
    )
  }
}
const mapStateToProps = (state)=>{
  return {
    items: state.products.items
  }
}
const mapDispatchToProps= (dispatch)=>{

  return{
    addToCart: (id)=>{dispatch(addToCart(id))}
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductsList)
