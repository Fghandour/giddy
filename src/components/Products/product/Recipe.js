import React, { Component } from 'react'
import { connect } from 'react-redux'
import Icon from "react-eva-icons";
import { Redirect } from 'react-router-dom'
import {sendNotification} from '../functions'
class Recipe extends Component{
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state={
      loginStatus:false
    }
  }
  componentWillUnmount() {
    // if(this.refs.shipping.checked)
    //   this.props.substractShipping()
  }

  // handleChecked = (e)=>{
  //   if(e.target.checked){
  //     this.props.addShipping();
  //   }
  //   else{
  //     this.props.substractShipping();
  //   }
  // }
  async submitForm(){
     if(this.props.user.isValidate){
       var notification={
         email:this.props.user.user.email,
         products: this.props.addedItems,
         TotalPrice:this.props.total
       }
       var data = await sendNotification(notification)
     }
     else{
       this.setState({loginStatus: true})
     }

  }
  render(){
    if(this.state.loginStatus){
      return <Redirect to='/sign-in'  />
    }
    return(
      <div className="container">
        <div className="collection">
          {/*<li className="collection-item">*/}
          {/*  <label>*/}
          {/*    <input type="checkbox" ref="shipping" onChange= {this.handleChecked} />*/}
          {/*    <span>Shipping(+6$)</span>*/}
          {/*  </label>*/}
          {/*</li>*/}
          <li className="collection-item"><b>Total: {this.props.total} $</b></li>
        </div>
        <div className="checkout">
          <button type="submit" className="btn btn-primary" onClick={this.submitForm}>Checkout</button>

        </div>
      </div>
    )
  }
}

const mapStateToProps = (state)=>{
  return{
    addedItems: state.products.addedItems,
    total: state.products.total,
    user:state.user
  }
}

const mapDispatchToProps = (dispatch)=>{
  return{
    addShipping: ()=>{dispatch({type: 'ADD_SHIPPING'})},
    substractShipping: ()=>{dispatch({type: 'SUB_SHIPPING'})}
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Recipe)
