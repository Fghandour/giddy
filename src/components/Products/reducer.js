import Item1 from '../../images/item1.jpg'
import Item2 from '../../images/item2.jpg'
import Item3 from '../../images/item3.jpg'
import Item4 from '../../images/item4.jpg'
import Item5 from '../../images/item5.jpg'
import Item6 from '../../images/item6.jpg'
import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY,ADD_SHIPPING } from './actionTypes'
const initState = {
  items: [
    {id:1,title:'Alexander', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110,img:"https://www.cdiscount.com/pdt2/9/6/2/1/700x700/mp17347962/rw/baskets-alexander-mcqueen-chaussures-de-course-cha.jpg"},
    {id:2,title:'Adidas', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:80,img: "https://www.cdiscount.com/pdt2/3/8/1/1/700x700/mp07894381/rw/chaussettes-chaussures-homme-2018-hiver-nouvelle-m.jpg"},
    {id:3,title:'Vans', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:120,img: "https://media.intersport.fr/is/image/intersportfr/IUNOLN_ADF_Q1?$produit_l$&$product_grey$"},
    {id:4,title:'Dhresource', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:260,img:"https://www.dhresource.com/0x0/f2/albu/g10/M00/F4/B9/rBVaVlxCi0iAWHVLAATcuZPf2k0975.jpg"},
    {id:5,title:'Packshot', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:160,img: "https://img01.ztat.net/article/TA/11/1A/20/ID/11/TA111A20I-D11@2.jpg?imwidth=606&filter=packshot"},
    // {id:6,title:'Blues', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:90,img: Item6}
  ],
  addedItems:[],
  total: 0

}
const cartReducer= (state = initState,action)=>{

  //INSIDE HOME COMPONENT
  if(action.type === ADD_TO_CART){
    let addedItem = state.items.find(item=> item.id === action.id)
    //check if the action id exists in the addedItems
    let existed_item= state.addedItems.find(item=> action.id === item.id)
    if(existed_item)
    {
      addedItem.quantity += 1
      return{
        ...state,
        total: state.total + addedItem.price
      }
    }
    else{
      addedItem.quantity = 1;
      //calculating the total
      let newTotal = state.total + addedItem.price

      return{
        ...state,
        addedItems: [...state.addedItems, addedItem],
        total : newTotal
      }

    }
  }
  if(action.type === REMOVE_ITEM){
    let itemToRemove= state.addedItems.find(item=> action.id === item.id)
    let new_items = state.addedItems.filter(item=> action.id !== item.id)

    //calculating the total
    let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity )
    console.log(itemToRemove)
    return{
      ...state,
      addedItems: new_items,
      total: newTotal
    }
  }
  //INSIDE CART COMPONENT
  if(action.type=== ADD_QUANTITY){
    var itemToUpdate= state.addedItems.find(item=> action.id === item.id)
    var newTotal = state.total+ itemToUpdate.price

    let newList = state.addedItems.map(item => (
      item.id===action.id ? {...item, quantity: item.quantity += 1}: item
    ))
    return{
      ...state,
      addedItems:newList,
      total: newTotal
    }
  }
  if(action.type=== SUB_QUANTITY){
    let addedItem = state.items.find(item=> item.id === action.id)
    //if the qt == 0 then it should be removed
    if(addedItem.quantity === 1){
      let new_items = state.addedItems.filter(item=>item.id !== action.id)
      let newTotal = state.total - addedItem.price
      return{
        ...state,
        addedItems: new_items,
        total: newTotal
      }
    }
    else {
      addedItem.quantity -= 1
      let newTotal = state.total - addedItem.price
      return{
        ...state,
        total: newTotal
      }
    }

  }

  if(action.type=== ADD_SHIPPING){
    return{
      ...state,
      total: state.total + 6
    }
  }

  if(action.type=== 'SUB_SHIPPING'){
    return{
      ...state,
      total: state.total - 6
    }
  }

  else{
    return state
  }

}

export default cartReducer
