
import React,{ useState } from 'react';
import Icon from "react-eva-icons";
import ReactTooltip from "react-tooltip";
import mobileApp from "../../images/mobileApp.gif";


function ConfirmPayment(props) {

  return (
    <>

      <div className="container ">

        <div className="card mt-2 border-warning">
          <div className="card-body row">
            <div className="d-flex col-1 justify-content-end">
              <Icon
                name="alert-circle-outline"
                size="xlarge"     // small, medium, large, xlarge
                fill={"orange "}
              />
            </div>
            <div className="col-11 text-warning">
              Pourquoi nous vous demandons de confirmer votre identité?
              <p> Dans le cadre de la directive européenne relative aux services de paiment (DSP2),le niveau de sécurité de l'accés à votre Espace Client est renforcé.
              </p>
              <p> Pour accéder à vos comptes merci de confimer votre identité depuis votres smartphone ou votre tablette avec Confirmation Mobile
              </p>
            </div>
          </div>

        </div>
        <div className="card mt-2 h-75">
          <div className="card-body text-center">
            <Icon
              name="lock"
              size="xlarge"     // small, medium, large, xlarge
              fill={"black "}
            />
            Votre connexion nécessite une sécurisation
            <p>
              Démarrez votre application mobile CIC depuis votre appareil pour vérifier et confirmer votre identité
            </p>
            <img src={mobileApp}/>
          </div>

        </div>
      </div>
    </>

  );
}
export default (ConfirmPayment);
