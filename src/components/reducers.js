import { combineReducers, createStore  } from 'redux';
import  userReducer  from './userReducer/reducer';
import cartReducer from './Products/reducer'
const rootReducer = combineReducers({
  user: userReducer,
  products:cartReducer
});

export default rootReducer;
