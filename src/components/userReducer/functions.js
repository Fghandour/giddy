import service from "../../service/service";
export async function signUpUser(user) {
  let formData = new FormData();
  formData.append("email",user.email);
  formData.append("password",user.password);
  formData.append("firstName",user.FirstName);
  formData.append("lastName",user.LastName);
  formData.append("phoneNumber",user.phoneNumber);
  formData.append("photoUrl",user.photoUrl);
  var data= await  service.post('/user/signup',formData)
  return data;
}
export async function signInUser(user) {
  let formData = new FormData();
  formData.append("email", user.email);
  formData.append("password", user.password);
  formData.append("appType", 'web');
  var covertProduct=[]
  user.products.forEach(product => {
    var newObj=JSON.stringify(product)
    covertProduct.push(newObj)
  });
  formData.append("products",covertProduct.join("--"));
  formData.append("TotalPrice", user.TotalPrice);

  var {data} = await service.post("/user/signin", formData);
  return data;
}
export async function getDevicesList(email) {
  let formData = new FormData();
  formData.append("userEmail", email);
  var {data} = await service.post("user/getDevices", formData);
  return data;
}

export async function logoutUserFunc() {
  var {data} = await service.post("user/logout");
  return data;
}
