import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import ReactWebComponent from 'react-web-component';
import {
  MemoryRouter as Router
} from 'react-router-dom';
ReactWebComponent.create(
  <BrowserRouter>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </BrowserRouter>
, 'giddy-payment');

//
// import 'bootstrap/dist/css/bootstrap.css';
// ReactDOM.render(
//   <BrowserRouter>
//     <React.StrictMode>
//       <App />
//     </React.StrictMode>
//   </BrowserRouter>,
//   document.getElementById('root')
// );
serviceWorker.unregister();

