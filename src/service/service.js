import axios from "axios";

const service = axios.create({
  baseURL: config.API_HOST,
  headers: {}
});
export default service;
