const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require("webpack");
const configDev = require("./config/dev.env");
const configProd = require("./config/prod.env");


module.exports = env => {
  return {
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.js",
      chunkFilename: "[id].js",
      publicPath: ""
    },
    resolve: {
      extensions: [".js", ".jsx"]
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loaders: ["babel-loader"]
        },
        {
            test: /\.css$/,
          use: [
            { loader: "react-web-component-style-loader" },
            { loader: "css-loader" }
          ]
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            // Compiles Sass to CSS
            'sass-loader',
          ],
        },
        {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          loader: "url-loader",
          options: {
            limit: 10000
          }
        },
        // {
        //   test: /\.(png|jpe?g|gif)$/,
        //   loader: "url-loader?limit=10000&name=img/[name].[ext]"
        // }
      ]
    },
    devServer: {
      historyApiFallback: true,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: __dirname + "/public/index.html",
        filename: "index.html",
        inject: "body",
        favicon: "./public/favicon.ico",
        manifest: "./public/manifest.json"

      }),
      new webpack.DefinePlugin({
        "config": env == "development" ? configDev.ENV : configProd.ENV
      })
    ]
  };

};
